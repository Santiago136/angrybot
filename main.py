import time
import sys
from VkBot import VkBot
from settings import Settings


def main():
    # initializing
    setting = Settings()
    setting.load_auth_data()
    print('Bot run with name: ' + str(setting.name) + '\npassword : ' + str(setting.password))

    _type = 'chat'
    _id = setting.chat_id

    if len(sys.argv) > 1:
        if sys.argv[1] == 'user':
            _type = 'user'
            _id = setting.user_id

    bot = VkBot(setting.name, setting.password, _type, _id)
    print('links to bot:' + str(sys.getrefcount(bot)))

    last_answered_msg = bot.get_last_msg()

    # Bot's action
    while True:
        try:
            tmp_last_msg = bot.get_last_msg()
            if tmp_last_msg != '':
                if last_answered_msg == '' or (last_answered_msg['body'] != tmp_last_msg['body']):
                    last_answered_msg = tmp_last_msg
                    if last_answered_msg != '':
                        bot.generate_response(last_answered_msg)
                        if last_answered_msg['body'].lower().find('отключись') != -1:
                            print('Shutting down...')
                            break
            time.sleep(1)

        except Exception as ex:
            bot.send_msg('Что-то пошло не так: ' + str(ex))
            print(ex)
            break

    # del bot
    bot.stop()


if __name__ == '__main__':
    main()

