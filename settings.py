import json


class Settings:
    def __init__(self):
        self.name = ''
        self.password = ''
        self.user_id = ''
        self.chat_id = ''

    def load_auth_data(self):
        setting_data = json.loads(open('auth.json').read())
        self.name = setting_data['auth_data']['name']
        self.password = setting_data['auth_data']['password']
        self.user_id = setting_data['target_data']['user_id']
        self.chat_id = setting_data['target_data']['chat_id']
