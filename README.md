# README #

This is example of chat bot for vk.com
Python version: 3.6

### How to run in console ###

For chat mode input:

* python main.py

For user mode input:

* python main.py user

### Full project include files ###

* main.py - entering to the program
* VkBot.py - main class that organize bot's logic
* WordWorker.py - class that parse user's answers
* Replicas.py - class that helps to create bot's answer
* settings.py - class that helps to load bot's login data
* auth.json - login data
* vocabulary.json - pool of replics