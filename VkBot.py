import vk_api
import random
import threading
from WordWorker import WordWorker
from Replicas import Replicas


class VkBot:
    _vk_api = None
    answers = Replicas()
    
    mode = 'speaking'
    orientation = 'neutral'
    chat_id = 0
    user_id = 0

    def __init__(self, _bot_login, _bot_password, _type, _id):
        self._vk_api = vk_api.VkApi(login=_bot_login, password=_bot_password)     # Write login and password
        self._vk_api.auth()
        
        if _type == 'user':
            self.user_id = _id
            print('User mode')
        elif _type == 'chat':
            self.chat_id = _id
            print('Chat mode')
        
        self.send_msg('Приветствую! Как на счет пообщаться?')

        # TODO can be moved to start_check_opponent(). but not now
        self.life_checker = threading.Timer(random.randint(30, 360), self.check_opponent_activity)
        self.life_checker.start()
        self.answers.load_answers('default')
        print('Bot initialized')
        
    def stop(self):
        if self.life_checker.is_alive():
            self.life_checker.cancel()
        self.send_msg('Я ушел.')
        print('Bot deleted')

    def generate_response(self, _last_msg):
        try:
            self.start_check_opponent()
            
            # TODO WordWorker should return dictionary { "person": , "meaning": , "to": , "type": } or something else

            person = WordWorker.detect_person(_last_msg['body'])
            if person != 'unknown':
                self.answers.load_answers(person)
            
            message_type = WordWorker.get_message_type(_last_msg['body'])
            print('string : ' + str(message_type))
            if message_type == 'silent' or message_type == 'speaking':
                self.mode = message_type
                return
            if self.mode == 'silent':
                rnd_number = random.randint(0, 4)
                if rnd_number < 2:
                    self.send_msg(self.answers.silent_answers[rnd_number])
                return

            if message_type == 'greeting':
                u_info = self._vk_api.method('users.get', {'user_id': _last_msg['user_id']})
                name = str(u_info[0]['first_name'])
                rnd_number = random.randint(0, len(self.answers.greeting_answers) - 1)
                self.send_msg(name + ', ' + self.answers.greeting_answers[rnd_number])
                return

            if message_type in self.answers.yes_no_answers:
                self.send_msg(self.answers.yes_no_answers[message_type])
                return
            if message_type in self.answers.question_answers:
                self.send_msg(self.answers.question_answers[message_type])
                return

            if message_type in 'alone':
                answer = WordWorker.get_dick_rhyme(_last_msg['body'])
                if answer == 'dick':
                    rnd_number = random.randint(0, len(self.answers.dick_answers) - 1)
                    self.send_msg(self.answers.dick_answers[rnd_number])
                else:
                    self.send_msg(answer)
                return

            if message_type == 'unknown':
                u_info = self._vk_api.method('users.get', {'user_id': _last_msg['user_id']})
                name = str(u_info[0]['first_name'])
                rnd_number = random.randint(0, len(self.answers.random_answers) - 1)
                self.send_msg(name + ', ' + self.answers.random_answers[rnd_number])
                return
                
            return
        except Exception as ex:
            self.send_msg('Что-то пошло не так: ' + str(ex))

    # wrapper for send message
    def send_msg(self, _msg):
        if self.user_id != 0:
            self.send_to_user(_msg)
            return
        elif self.chat_id != 0:
            self.send_to_chat(_msg)
            return

    # wrapper for get last message
    def get_last_msg(self):
        if self.user_id != 0:
            print('User get last msg')
            return self.get_last_msg_from_user()
        elif self.chat_id != 0:
            print('Chat get last msg')
            return self.get_last_msg_from_chat()
    
    # send private message _string to user
    def send_to_user(self, _string):
        u_info = self._vk_api.method('users.get', {'user_id': self.user_id})  # Get users info
        name = str(u_info[0]['first_name'])
        self._vk_api.method('messages.send', {'user_id': self.user_id, 'message': _string})

    # send message _string to chat
    def send_to_chat(self, _string):
        self._vk_api.method('messages.send', {'chat_id': self.chat_id, 'message': _string})

    # get last msg from user
    def get_last_msg_from_user(self):
        response = self._vk_api.method('messages.get', {'out': 0, 'count': 35, 'time_offset': 3600})
        for msg in response['items']:
            if msg['user_id'] == self.user_id and msg['title'] == ' ... ':
                return msg
        return ''

    # get last msg from chat
    def get_last_msg_from_chat(self):
        response = self._vk_api.method('messages.get', {'out': 0, 'count': 20, 'time_offset': 3600, 'last_message_id' : 1})
        for msg in response['items']:
            if msg['title'] != ' ... ' and str(msg['chat_id']) == str(self.chat_id):
                print('RETURNING MESSAGE: ' + str(msg['title']) + ': ' + str(msg['body']))
                return msg
        return ''

    # functions for check opponent status
    def start_check_opponent(self):
        if self.life_checker.is_alive():
            self.life_checker.cancel()
        self.life_checker = threading.Timer(random.randint(30, 360), self.check_opponent_activity)
        self.life_checker.start()
        
    def check_opponent_activity(self):
        print('Activity timer is expired')
        rnd_number = random.randint(0, len(self.answers.life_check_questions) - 1)
        self.send_msg(self.answers.life_check_questions[rnd_number])
        # continue check
        self.start_check_opponent()
